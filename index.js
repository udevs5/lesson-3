// Lesson 3
function fizzBuzz () {
    for(let i = 1; i<=100; i++) {
        if(i%15==0) {
            console.log("FizzBuzz")
        } else if(i%3==0) {
            console.log("Fizz")
        } else if(i%5==0) {
            console.log("Buzz")
        } else {
            console.log(i)
        }
    }
}

function filterArray (arr) {
    let tmp = arr.filter( (el)=>Array.isArray(el) ).reduce((total, current) => {
        total.push(...current);
        return total;
    }, []);
    tmp.sort();
    return tmp;
}

// Output DON'T TOUCH!
console.log(fizzBuzz())
console.log(filterArray([ [2], 23, 'dance', true, [3, 5, 3] ]))